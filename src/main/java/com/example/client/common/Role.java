package com.example.client.common;

public enum Role {
    ADMIN,
    USER,
    ANONYMOUS;

    Role() {
    }
}
