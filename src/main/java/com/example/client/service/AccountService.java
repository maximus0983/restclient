package com.example.client.service;

import com.example.client.common.Account;
import com.example.client.common.CustomUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class AccountService {
    @Autowired
    private UserRepository userRepository;

    public UserDetails getByUsername(String username) {
        Account account = userRepository.findByUsername(username);
        return new CustomUserDetails(account);
    }
}
