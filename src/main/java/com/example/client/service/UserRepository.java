package com.example.client.service;

import com.example.client.common.Account;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<Account, Long> {
    Account findByUsername(String username);
}
