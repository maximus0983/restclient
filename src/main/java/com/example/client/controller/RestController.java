package com.example.client.controller;

import com.example.client.common.Resource;
import com.example.client.service.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.annotation.MultipartConfig;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.CertificateException;

@Controller
@RequestMapping("/doc")
@MultipartConfig
public class RestController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RestTemplate restTemplate;

    @RequestMapping("/")
    public ModelAndView showResource(@RequestParam("id") long id) {
        Resource resource = restTemplate.getForObject("https://localhost:8443/document/" + id, Resource.class);
        ModelAndView modelAndView = new ModelAndView("docById");
        modelAndView.addObject("doc", resource);
        return modelAndView;
    }

    @GetMapping("/all")
    @ResponseBody
    public Resource[] docs() {
        return restTemplate.getForObject("https://localhost:8443/document/alldocs", Resource[].class);
    }

    @PostMapping("/upload")
    public String writeDoc(@RequestParam("file") MultipartFile file, ModelAndView modelAndView) throws IOException, KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException, CertificateException, InvalidKeyException, SignatureException {
        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(this.getClass().getResourceAsStream("/privateKey.p12"), "changeit".toCharArray());
        PrivateKey privateKey = (PrivateKey) keyStore.getKey("privateKey", "changeit".toCharArray());
        FileOutputStream fo;
        String tempFile = file.getOriginalFilename();
        fo = new FileOutputStream(tempFile);
        fo.write(file.getBytes());
        fo.close();
        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initSign(privateKey);
        byte[] messageBytes = file.getBytes();
        signature.update(messageBytes);
        byte[] digitalSignature = signature.sign();
        String digitalSignature2 = "digitalSignature2";
        Files.write(Paths.get(digitalSignature2), digitalSignature);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", new FileSystemResource(tempFile));
        body.add("digitalSign", new FileSystemResource(digitalSignature2));
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
        ResponseEntity<String> response = restTemplate.postForEntity("https://localhost:8443/document/fileupload", requestEntity, String.class);
        return "redirect:/";
    }
}