package com.example.client.controller;

import com.example.client.common.Account;
import com.example.client.common.CustomUserDetails;
import com.example.client.common.Role;
import com.example.client.service.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping("/")
    public String showLoginPage() {
        return "homepage";
    }

    @GetMapping("/registration")
    public String showRegistrationForm() {
        return "registration";
    }

    @GetMapping("/docById")
    public String showDoc() {
        return "docById";
    }

    @GetMapping("/login")
    public String viewHomePage() {
        return "loginpage";
    }

    @PostMapping("/regController")
    public String registerUserAccount(@ModelAttribute Account account,
                                      BindingResult result) {

        Account existing = userRepository.findByUsername(account.getUsername());
        if (existing != null) {
            result.rejectValue("email", null, "There is already an account registered with that email");
        }

        if (result.hasErrors()) {
            return "registration";
        }
        account.setRole(Role.USER);
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        userRepository.save(account);
        UserDetails accountForSecurity = new CustomUserDetails(account);
        Authentication auth = new UsernamePasswordAuthenticationToken(accountForSecurity, null,
                accountForSecurity.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);
        return "redirect:/";
    }
}