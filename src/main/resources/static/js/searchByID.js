$(function () {
    $('#showdoc').click(function () {
        var id = $('#id').val();
        $.ajax({
            url: "doc/" + id,
            type: "GET",
            dataType: "json",

            data: {
                id: id,
            },
            success: function (data) {
                var html = "<h2>" + data.id + " " + data.text + "</h2>"
                $("#result").html(html);
            }
        })
    })

});